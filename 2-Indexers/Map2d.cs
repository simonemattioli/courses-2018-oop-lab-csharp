﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {

        Dictionary<Tuple<TKey1, TKey2>, TValue> map;

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return map.Equals(other);
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                TValue res;
                this.map.TryGetValue(Tuple.Create(key1, key2), out res);
                return res;
            }
            set { this.map.Add(Tuple.Create(key1, key2), value); }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            List<Tuple<TKey2, TValue>> row = new List<Tuple<TKey2, TValue>>();
            foreach (var item in map)
            {
                if (item.Key.Item1.Equals(key1))
                {
                    row.Add(Tuple.Create(item.Key.Item2, item.Value));
                }
            }
            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            List<Tuple<TKey1, TValue>> row = new List<Tuple<TKey1, TValue>>();
            foreach (var item in map)
            {
                if (item.Key.Item1.Equals(key2))
                {
                    row.Add(Tuple.Create(item.Key.Item1, item.Value));
                }
            }
            return row;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            List<Tuple<TKey1, TKey2, TValue>> row = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (var item in map)
            {
                    row.Add(Tuple.Create(item.Key.Item1,item.Key.Item2, item.Value));
            }
            return row;
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            
        }

        public int NumberOfElements
        {
            get
            {
                return map.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
