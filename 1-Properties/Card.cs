﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string GetSeed
        {
            get { return this.seed; }
        }

        // TODO improve
        public string GetName
        {
            get { return this.name; }
        }

        // TODO improve
        public int GetOrdinal
        {
            get { return this.ordial; }
        }

        public override string ToString()
        {
            return $"{GetType().Name}(Name={GetName}, Seed={GetSeed}, Ordinal={GetOrdinal})"; 
        }

        public override bool Equals(object obj)
        {
            return (this == obj);
        }

        public override int GetHashCode()
        {
            // TODO improve
            return seed.GetHashCode() + name.GetHashCode() + ordial.GetHashCode(); 
        }
    }

}